import React from "react";

function AttendeesList(props) {
    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Conference</th>
                </tr>
            </thead>
            <tbody>

                {/* {for (let attendee of props.attendees) {
                    <tr>
                        <td>{attendee.name}</td>
                        <td>{attendee.conference}</td>
                    </tr>
                } */}
                {props.attendees.map(attendee => {
                    // <React.Fragment>
                    return (
                        <tr key={attendee.href}>
                            <td>{attendee.name}</td>
                            <td>{attendee.conference}</td>
                        </tr>
                    );
                    // </React.Fragment>

                })}
            </tbody>
        </table>
    );
}

export default AttendeesList;